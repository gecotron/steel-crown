/*
 * Created by Michael Ward on 23/04/24
 * Licensed under the Mozilla Public License Version 2.0
 * -----------------------------------------------------
 * Header file for Menus & UI. Builds on the rayGui library.
 * Contains definitions UI related behaviour.
 */

#include "ui.hpp"

// Constructors & Destructors

/// @brief Default constructor for UI elements. Initialises generic (useless) element at (0, 0)
ui::UiElement::UiElement(void) { position = raylib::Vector2(0, 0); }
/// @brief Simple constructor for the skeleton UI element at a given position
/// @param pos raylib 2d vector
ui::UiElement::UiElement(raylib::Vector2 pos) { position = pos; }
/// @brief Base destructor for UI elements. Does nothing but it's best
ui::UiElement::~UiElement(void) {}

// Getters & Setters ----

/// @brief Set element position based on raylib 2d vector
/// @param pos raylib 2d vector position
void ui::UiElement::Position(const raylib::Vector2 &pos) { position = pos; }

/// @brief Set element position based on float variables
/// @param x float of x position
/// @param y float of y position
void ui::UiElement::Position(float x, float y) {
	position.SetX(x);
	position.SetY(y);
}

/// @brief Gets element position as raylib 2d vector
/// @returns 2d vector of current position of element
raylib::Vector2 ui::UiElement::Position(void) { return this->position; }

// Methods ----

/// @brief Function to render UI element, which draws & displays its given content. Designed to be overloaded by child classes
void ui::UiElement::Render(void) { raylib::DrawText("ERR", position.GetX(), position.GetY(), 40, raylib::Color::Red()); }

/*
 * Created by Michael Ward on 22/04/24
 * Licensed under the Mozilla Public License Version 2.0
 * -----------------------------------------------------
 * Source file for game entities. Includes all intractables, enemies and the player.
 * Contains definitions for entity related behaviour.
 */

#include "entities.hpp"

// class Entity ----

/// @brief Default Basic Constructor
entities::Entity::Entity(void) { entities::activeEntities += 1; }
/// @brief Deconstructor
entities::Entity::~Entity(void) { entities::activeEntities -= 1; }

/// @brief Copy position of another vector
/// @param pos copy of raylib vector
void entities::Entity::Position(const raylib::Vector3 &pos) { this->position = pos; }

/// @brief Set vector position
/// @param x
/// @param y
/// @param z
void entities::Entity::Position(float x, float y, float z) {
	this->position.SetX(x);
	this->position.SetY(y);
	this->position.SetZ(z);
}

/// @brief Getter for entity position
/// @return Returns raylib vector of current entity position
raylib::Vector3 entities::Entity::Position(void) { return this->position; }

void entities::Entity::Id(int i) { this->id = i; }

int entities::Entity::Id(void) { return this->id; }

void entities::Entity::Render(void) { raylib::DrawText("ERR", position.GetX(), position.GetY(), 40, raylib::Color::Red()); }

// --------

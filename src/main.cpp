/*
 * Created by Michael Ward on 22/04/24
 * Licensed under the Mozilla Public License Version 2.0
 * -----------------------------------------------------
 * Entry point for Steel Crown
 */

// CMake/System level include files ----
#include <cstring>
#include "raylib-cpp.hpp"

// Project Headers ----
#include "scene.hpp"
#include "log.hpp"

int main(int argc, char *argv[]) {
	// Parse CLI args ----
	if (argc > 1) {
		if (strcmp(argv[1], "debug") == 0) {
			console::debug::debugging = true;
			console::debug::log("now debugging", console::logLevel::warning);
		}
	}

	// Initialise Window ----
	int width = 960;  // These values are temporary for development
	int height = 540; // Later on the screen will be made to be resized.
	raylib::Window window(width, height, "STEEL CROWN");
	// DisableCursor(); // Focus cursor onto screen when playing a level.

	// Base Variables ----
	int delta = 60;		  // deltaTime/frameRate
	int frameCounter = 0; // Timer variable

	// Create Camera ----
	raylib::Camera camera(raylib::Vector3(0.0f, 5.0f, 10.0f), raylib::Vector3(0.0f, 0.0f, 0.0f), raylib::Vector3(0.0f, 1.0f, 0.0f), 45.0f, CAMERA_PERSPECTIVE);

	// Demo Cube ----
	raylib::Vector3 cubePosition(0.0f, 0.0f, 0.0f);

	// Set Target Framerate ----
	window.SetTargetFPS(delta);

	scene::sceneType currentScene = scene::sceneType::mainMenu;

	// Game Loop ----
	while (!window.ShouldClose()) { // Detect Window Close Button or Esc key press

		// Update Camera & Window System ----
		camera.Update(CAMERA_ORBITAL);

		// Game Rendering ----
		window.BeginDrawing();
		{
			window.ClearBackground(raylib::Color::Black());

			// Begin 3D rendering ----
			camera.BeginMode();
			{
				DrawCube(cubePosition, 2.0f, 2.0f, 2.0f, raylib::Color::Red());
				DrawCubeWires(cubePosition, 2.0f, 2.0f, 2.0f, raylib::Color::Maroon());
			}
			camera.EndMode();

			// Begin 2d/UI rendering ----
			raylib::DrawText("steel - crown", 10, 10, 45, raylib::Color::Maroon());
			if (console::debug::debugging) {
				window.DrawFPS(830, 50);
			}
		}
		window.EndDrawing();
	}

	return 0;
}

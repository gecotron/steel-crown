/*
 * Created by Michael Ward on 23/04/24
 * Licensed under the Mozilla Public License Version 2.0
 * -----------------------------------------------------
 * Header file for logging utilities.
 * Contains declarations for logging and debugging.
 */

#include <iostream>

namespace console {

enum logLevel { normal, warning, error };

static logLevel level = normal;

std::string levelToString(logLevel lvl);

void log(std::string msg);

void log(std::string msg, logLevel lvl);

// namespace specifically for debugging
namespace debug {
static bool debugging = false;

void log(std::string msg);

void log(std::string msg, logLevel lvl);

} // namespace debug
} // namespace console

/*
 * Created by Michael Ward on 22/04/24
 * Licensed under the Mozilla Public License Version 2.0
 * -----------------------------------------------------
 * Controls scene management of the game.
 * Contains declarations for managing & using various scenes, such as menus & levels.
 */

#include <vector>
#include "ui.hpp"
#include "entities.hpp"
namespace scene {

// sceneType Enum tracks the type of scene currently used.
enum sceneType { loadingScreen, mainMenu, levelSelect, level, settings };

class Scene {
  public:
	virtual void Render(void);
}; // class Scene

class Menu : public Scene {
  private:
	std::vector<ui::UiElement *> elements;

  public:
	Menu(void);
	void Render(void);
};

class Level : public Scene {};
} // namespace scene

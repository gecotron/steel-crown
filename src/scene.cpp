/*
 * Created by Michael Ward on 22/04/24
 * Licensed under the Mozilla Public License Version 2.0
 * -----------------------------------------------------
 * Controls scene management of the game.
 * Contains definitions for managing & using various scenes, such as menus & levels.
 */

#include "scene.hpp"

/// @brief Render(void) virtual function.
/// Should be overridden by children of Scene class
void scene::Scene::Render(void) {}

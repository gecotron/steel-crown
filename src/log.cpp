/*
 * Created by Michael Ward on 23/04/24
 * Licensed under the Mozilla Public License Version 2.0
 * -----------------------------------------------------
 * Header file for logging utilities.
 * Contains definitions for logging and debugging.
 */

#include "log.hpp"

using namespace console;

/// @brief Helper function to convert logLevel enum to string
/// @param lvl logLevel enum to be parsed
/// @return c++ string value for converted enum
std::string console::levelToString(console::logLevel lvl) {
	switch (lvl) {
	case console::logLevel::normal:
		return "nor";
		break;
	case console::logLevel::warning:
		return "wrn";
		break;
	case console::logLevel::error:
		return "err";
	default:
		return "bad";
	}
}

/// @brief Print utility to log messages, and display what level of a problem it is at
/// @param msg String message to pass
void console::log(std::string msg) {
	std::string lvl;
	switch (console::level) {
	case console::logLevel::normal:
		lvl = "nor";
		break;
	case console::logLevel::warning:
		lvl = "wrn";
		break;
	case console::logLevel::error:
		lvl = "err";
	default:
		lvl = "bad";
	}

	std::cout << "[" << lvl << "] - " << msg << std::endl;
}

/// @brief Print utility with a specific level of error to log at
/// @param msg String message to pass
/// @param lvl level of error
void console::log(std::string msg, console::logLevel lvl) { std::cout << "[" << console::levelToString(lvl) << "] - " << msg << std::endl; }

/// @brief Used to log message specifically when debugging.
/// @param msg String message to pass
void console::debug::log(std::string msg) {
	if (console::debug::debugging) {
		console::log(msg);
	}
}

/// @brief Used to log message specifically when debugging, with a specific level of error to log at
/// @param msg String message to pass
/// @param lvl level of error
void console::debug::log(std::string msg, console::logLevel lvl) {
	if (console::debug::debugging) {
		console::log(msg, lvl);
	}
}

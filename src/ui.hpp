/*
 * Created by Michael Ward on 23/04/24
 * Licensed under the Mozilla Public License Version 2.0
 * -----------------------------------------------------
 * Header file for Menus & UI. Builds on the rayGui library.
 * Contains declarations UI related behaviour.
 */

#include "raylib-cpp.hpp"
#include <string>

namespace ui {
class UiElement {
  protected:
	raylib::Vector2 position;

  public:
	// Constructors & Destructors ----
	UiElement(void);
	UiElement(raylib::Vector2 pos);
	~UiElement(void);
	// Getters & Setters ----
	void Position(const raylib::Vector2 &pos);
	void Position(float x, float y);
	raylib::Vector2 Position(void);
	// Methods ----
	virtual void Render(void);
};
} // namespace ui

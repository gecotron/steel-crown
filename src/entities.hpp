/*
 * Created by Michael Ward on 22/04/24
 * Licensed under the Mozilla Public License Version 2.0
 * -----------------------------------------------------
 * Header file for game entities. Includes all intractables, enemies and the player.
 * Contains declarations for entity related behaviour.
 */

#include "raylib-cpp.hpp"

namespace entities {
static unsigned int activeEntities = 0; // Used to track currently active entities in a scene

class Entity {
  protected:
	raylib::Vector3 position;
	int id;

  public:
	// Constructors & Destructors ----
	Entity(void);
	~Entity(void);
	// Getters & Setters ----
	raylib::Vector3 Position(void);
	void Position(const raylib::Vector3 &pos);
	void Position(float x, float y, float z);
	int Id(void);
	void Id(int i);
	// Methods ----
	virtual void Render(void);

}; // class Entity

} // namespace entities

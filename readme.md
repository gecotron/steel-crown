# Steel Crown

Old machines groan to life and rise again amidst the ashes of a dying kingdom.
An automaton of the past awakes, prepared to create a perfect civilisation from the ashes of its conquest.

That which wears a crown of steel is to rise again.
Will you rise to stop it?

## About

Steel Crown is 3D fps inspired by both old and new movement shooters, written in good old c/c++ with raylib.

## Building

Steel Crown is a cmake project, and has its requirements listed below

Required:

- cmake
- ninja

Optional:

- just

If you have the just command runner, type `just` or `just run` to compile the project to the `bin/` directory
> running `just run` will also run the program once it compiles

Otherwise, make a directory named `bin` cd into it, run `cmake ..` and then `ninja`.
The program will compile to a binary named steel-crown (or steel-crown.exe if you are on windows)
